
# Get readable list of network IPs
alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias flush="dscacheutil -flushcache" # Flush DNS cache
alias ovftool="/Applications/VMware\ OVF\ Tool/ovftool"

function parse_git_branch {
   git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

GRADLE_HOME=/Users/djbelieny/projects/tools/gradle-2.4;
export GRADLE_HOME

export PS1="\h:\W \$(parse_git_branch)$ "
export PATH="/usr/local/bin:$HOME/.rvm/bin:$HOME/rpi/arm-cs-tools/bin:$GRADLE_HOME/bin:$PATH" #Add rpi emulator tools to path

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
