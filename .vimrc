execute pathogen#infect()
syntax on
colorscheme itg_flat
set paste
set guifont=Sauce\ Code\ Powerline:h13
set backup
set backupdir=~/.vim/backup
set directory=~/.vim/tmp
set cul
"hi CursorLine term=none cterm=none ctermbg=3
set background=dark
set ruler                     " show the line number on the bar
set more                      " use more prompt
set autoread                  " watch for file changes
set number                    " line numbers
set hidden
set lazyredraw                " don't redraw when don't have to
set showmode
set showcmd
set nocompatible              " vim, not vi
set autoindent smartindent    " auto/smart indent
set smarttab                  " tab and backspace are smart
set tabstop=2                 " 6 spaces
set shiftwidth=2
set scrolloff=5               " keep at least 5 lines above/below
set sidescrolloff=5           " keep at least 5 lines left/right
set history=200
set backspace=indent,eol,start
set linebreak
set cmdheight=1               " command line two lines high
set undolevels=10000           " 1000 undos
set updatecount=100           " switch every 100 chars
set complete=.,w,b,u,U,t,i,d  " do lots of scanning on tab completion
set ttyfast                   " we have a fast terminal
set noerrorbells              " No error bells please
set shell=bash
set fileformats=unix
set ff=unix
filetype on                   " Enable filetype detection
filetype indent on            " Enable filetype-specific indenting
filetype plugin on            " Enable filetype-specific plugins
set wildmode=longest:full
set wildmenu                  " menu has tab completion
let maplocalleader=','        " all my macros start with ,
set laststatus=2
set noexpandtab
set autowrite
set tabpagemax=15
set updatecount=50
set makeprg=php\ -l\ %
set autoindent
set smartindent
set errorformat=%m\ in\ %f\ on\ line\ %l
set showmatch
set incsearch
set foldenable " Turn on folding
set foldmarker={,} " Fold C style code (useful with high foldlevel)
set foldmethod=syntax
set foldlevelstart=1 " Don't autofold anything (but I can still fold manually)
set foldopen=block,hor,jump,search,tag,insert " what movements to open folds on
set completeopt=menu,longest
set formatoptions=rq
set comments=sl:/*,mb:*,elx:*/
let g:explVertical=1
let g:explWinSize=35
set wmh=0
"  searching
set incsearch                 " incremental search
set ignorecase                " search ignoring case
set hlsearch                  " highlight the search
set showmatch                 " show matching bracket
set diffopt=filler,iwhite     " ignore all whitespace and sync

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:php_cs_fixer_level = "all"                  " which level ?
let g:php_cs_fixer_config = "default"             " configuration

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

let g:returnAppFlag = 1

let g:airline_powerline_fonts = 1

autocmd FileType php let php_folding=1

"function! CleverTab()
"	  if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
"			    return "\<Tab>"
"	  else
"			    return "\<C-X>\<C-O>"
"endfunction

"inoremap <S-Tab> <C-R>=CleverTab()<CR>
"map <C-J> <C-W>j<C-W>_
"map <C-K> <C-W>k<C-W>_

:nnoremap <Leader>q :Bdelete<CR>

" spelling
if v:version >= 700
  " Enable spell check for text files
  autocmd BufNewFile,BufRead *.txt setlocal spell spelllang=en
endif

" mappings
" toggle list mode
nmap <LocalLeader>tl :set list!<cr>
" toggle paste mode
nmap <LocalLeader>pp :set paste!<cr>

map <leader>nt :NERDTreeToggle<CR>
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)

" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
