# README #

This is the collection of my dot files and some other configurations
I use on my terminal and other applications

Check it all out for my bash profile and Vim setup. I have
included a bash script to help install Vim.pathogen and all
the Vim plugins i like to use.

Also under vim/colors directory you will find my favorite 
color schemes for Vim.


* 1- Fork out this repo
* 2- Clone it locally
``` 
git clone your_repo ~/dotfiles
```
* 2- Run the install_all.sh to install all of my favorite plugins
```
cd ~/dotfiles
chmod +x install_all.sh
./install_all.sh
```
* 3- Backup your .vimrc and/or .gvimrc
```
cp ~/.vimrc ~/.vimrc.bkp
cp ~/.gvimrc ~/.gvimrc.bkp

```
* 4- Copy the .vimrc and/or .gvimrc to your home or link them 
to your home folder using:
```
cd ~
ln -s ~/dotfiles/.vimrc .vimrc
ln -s ~/dotfiles/.gvimrc .gvimrc
```
To install the colors you link the color folders to your vim install
```
ln -s ~/dotfiles/vim/colors ~/.vim/colors
```
You can do the same with my .bash_profile if you like
my mappings and whatnot.
