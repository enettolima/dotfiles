#!/bin/bash

#Install vim pathogen if it doesn't exist
if [ ! -d "~/.vim/autoload" ]; then
	mkdir -p ~/.vim/autoload
	curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
fi

#Setup bundles directory if it doesn't exist
if [ ! -d "~/.vim/bundle" ]; then
	mkdir -p ~/.vim/bundle
fi

#Temporary and Backup directories as required by my vimrc
if [ ! -d "~/.vim/tmp" ]; then
	mkdir -p ~/.vim/tmp
fi
if [ ! -d "~/.vim/backup" ]; then
	mkdir -p ~/.vim/backup
fi

cd ~/.vim/bundle

git clone https://github.com/Raimondi/delimitMate
git clone https://github.com/scrooloose/nerdtree.git
git clone https://github.com/scrooloose/syntastic.git
git clone https://github.com/tomtom/tlib_vim.git
git clone https://github.com/MarcWeber/vim-addon-mw-utils.git
git clone https://github.com/bling/vim-airline
git clone https://github.com/junegunn/vim-easy-align.git
git clone https://github.com/stephpy/vim-php-cs-fixer.git
git clone https://github.com/garbas/vim-snipmate.git
git clone https://github.com/honza/vim-snippets.git
git clone https://github.com/bronson/vim-trailing-whitespace.git
git clone https://github.com/mattn/webapi-vim.git
git clone https://github.com/Yggdroot/indentLine.git
git clone https://github.com/fholgado/minibufexpl.vim
git clone git://github.com/tpope/vim-surround.git
git clone git://github.com/tpope/vim-repeat.git
git clone git://github.com/airblade/vim-gitgutter.git
git clone git://github.com/justinmk/vim-gtfo.git

cd ~

echo " "
echo "Do not forget to add 'execute pathogen#infect()' to the begining of your .vimrc and/or .gvimrc"
echo " "
